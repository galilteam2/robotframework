/*
* this function gets the pixel data for a certain area on the canvas
* @param canvasid number an integer which defines the nr of the canvas element on the page which needs to be used (starts at 0)
* @param start_x number an integer value which is the start position on the X-axis
* @param start_y number an integer value which is the start position on the Y-axis
* @param width number an integer value which defines the amount of pixels the move in the X-direction
* @param height number an integer value which defines the amount of pixels the move in the Y-direction
* @return string the array with result data is returned as a string
*/
window.GetRegionPixelData = function GetRegionPixelData(canvasid, start_x, start_y, width, height)
	{
       var canvas = document.getElementsByTagName("canvas");
       var context = canvas[canvasid].getContext("2d");
       //get pixel data in an array; we define the extraction area 
       var picture = context.getImageData(start_x, start_y, width, height);
       return picture.data;
   };
   //a return is needed by the Robot Framework
   return 1;