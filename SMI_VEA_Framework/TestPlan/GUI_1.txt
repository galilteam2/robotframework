*** Settings ***
Library           C:/Python27/Lib/site-packages/Selenium2Library
Library           ImapLibrary
Resource          ../Resources_Files/LoginPage.txt
Resource          ../Resources_Files/UsersKeyword.txt
Resource          ../Resources_Files/Home.txt
Resource          ../Resources_Files/Signup.txt
Library           ../Resources_Files/UltilityFunction.py
Library           Collections

*** Variables ***

*** Test Cases ***
Test1
    open browser    http://10.1.10.119:3000/index.html    Chrome
    Maximize Browser Window
    Login    Hung@speechmorphing.com    abc123

Verify the "login" link in the "Email taken�" message will navigate to VE Login screen
    open browser    http://10.1.10.119:3000/index.html    chrome
    Comment    maximize browser window
    sleep    5s
    click element    ${Link_Signup}
    sleep    10s
    signup    hung@speechmorphing.com    abc123    abc123
    ${errorMessage}=    GetText    ${Taken email message}
    log    ${Taken email message}
