*** Settings ***
Library           C:/Python27/Lib/site-packages/Selenium2Library

*** Variables ***
${Forgetpassword_subTitle}    xpath=/html/body/div/h1/b[2]    # Header
${FPEmailAddress_input}    user_name
${FPSubmit_btn}    SubmitButtonForgot
${ResetPassword_Message}    xpath=/html/body/div/h1
${FP_Login_Link}    link=Login
${EmailValidation_Message}    //*[@id="auth-form"]/div[1]/span
${Reset_inpox_email}    //*[@id="row_public_1470487053-20006803234-qqq"]/div[2]/div[5]/div    # //*[@id="row_public_1470486339-100017461118-qqq"]/div[2]/div[5]/div
${Reset_Password_link_email}    Reset Password    # //*[@id="ums-sect"]/div[1]/table/tbody/tr/td[2]/p[3]/a
${UMS_Password}    id=password    # //*[@id="password"]
${UMS_Confirm_Password}    //*[@id="password2"]
${UMS_Reset_btn}    //*[@id="enter"]    # \ //*[@id="reset_pwd_button"]
${UMS_PasswordsValidation_messag}    //*[@id='message1']    # xpath=/html/body/div/div/div/p[5]
${RPEmailvalidation_messag}    //*[@id="auth-form"]/div[1]/span    # css=.help-block.form-error
${ResetPassword_success_Message}    xpath=html/body/h4
